package Task3;

class Triangle {
    private double side1;
    private double side2;
    private double side3;

    // constructor with input parameters
    public Triangle(double side1, double side2, double side3) {
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    // method to calculate the perimeter of the triangle
    public double getPerimeter() {
        return side1 + side2 + side3;
    }

    // method to calculate the area of the triangle
    public double getArea() {
        double s = getPerimeter() / 2;
        return Math.sqrt(s * (s - side1) * (s - side2) * (s - side3));
    }

    public static void main(String[] args) {
        Triangle t1 = new Triangle(7,6,5);
        System.out.println("Perimeter of the triangle: " + t1.getPerimeter());
        System.out.println("Area of the triangle: " + t1.getArea());
    }
}

