package Task4;

class Counter {
    private int currentValue;
    private int minValue;
    private int maxValue;

    // Constructor with input parameters
    Counter(int minValue, int maxValue, int currentValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.currentValue = currentValue;
    }

    // Default constructor
    Counter() {
        minValue = 7;
        maxValue = 10;
        currentValue = 14;
    }

    // method to increase the counter
    public void increase() {
        if (currentValue < maxValue) {
            currentValue++;
        }
    }

    // method to decrease the counter
    public void decrease() {
        if (currentValue > minValue) {
            currentValue--;
        }
    }

    // method to get current counter value
    public int getCurrentValue() {
        return currentValue;
    }

    public static void main(String[] args) {
        // Initializing the counter with default values
        Counter c1 = new Counter();
        System.out.println("Counter: " + c1.getCurrentValue());
        c1.increase();
        System.out.println("Counter: " + c1.getCurrentValue());
        c1.decrease();
        System.out.println("Counter: " + c1.getCurrentValue());
        System.out.println("------------");

        // Initializing the counter with arbitrary values
        Counter c2 = new Counter(-8, 23, 17);
        System.out.println("Counter: " + c2.getCurrentValue());
        c2.increase();
        System.out.println("Counter: " + c2.getCurrentValue());
        c2.decrease();
        System.out.println("Counter: " + c2.getCurrentValue());
    }
}
