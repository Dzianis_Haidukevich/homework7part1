package Task1;

class Test1 {
    int var1;
    int var2;

    // Method for displaying the variables
    void display() {
        System.out.println("Var1: " + var1);
        System.out.println("Var2: " + var2);
    }

    // Methods for changing the variables
    void changeVar1(int newVal) {
        var1 = newVal;
    }

    void changeVar2(int newVal) {
        var2 = newVal;
    }

    // Method for finding the sum of the variables
    int findSum() {
        return var1 + var2;
    }

    // Method for finding the larger of the two variables
    int findLarger() {
        if (var1 > var2) {
            return var1;
        } else {
            return var2;
        }
    }

    public static void main(String[] args) {
        Test1 test = new Test1();
        test.changeVar1(3);
        test.changeVar2(17);
        test.display();
        System.out.println("Sum: " + test.findSum());
        System.out.println("Larger: " + test.findLarger());
    }
}
