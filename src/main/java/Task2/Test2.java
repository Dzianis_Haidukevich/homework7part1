package Task2;

class Test2 {
    private int var1;
    private int var2;

    // Constructor with input parameters
    Test2(int val1, int val2) {
        var1 = val1;
        var2 = val2;
    }

    // Default constructor
    Test2() {
        var1 = 0;
        var2 = 0;
    }

    // Set method for var1
    void setVar1(int newVal) {
        var1 = newVal;
    }

    // Set method for var2
    void setVar2(int newVal) {
        var2 = newVal;
    }

    // Get method for var1
    int getVar1() {
        return var1;
    }

    // Get method for var2
    int getVar2() {
        return var2;
    }

    public static void main(String[] args) {
        Test2 test = new Test2();
        test.setVar1(15);
        test.setVar2(41);
        System.out.println("Var1: " + test.getVar1());
        System.out.println("Var2: " + test.getVar2());
    }
}
